<!--
  one-line-summary

  This material has been released under and is subject to the terms of the
  Common Documentation License, Version 1.0 (the "License").
  The terms of which are hereby incorporated by reference.

  Please obtain a copy of the License in the included file,
  LICENSES/CDL-1.0.txt or online at:

      https://spdx.org/licenses/CDL-1.0.html

  Read the full License text before using this material. Your use of this
  material signifies your agreement to the terms of the License.

  Copyright: 2023 Drew Adams <druonysus@opensuse.org>
  License  : CDL-1.0
  Authors  : Drew Adams
-->

# xport

`xport` enables you to define "environment files" containing simple key-value pairs and generates the appropriate "export" syntax for various shells so you may use those as environment variables. This simplifies switching between different environments, and allows you to bulk set or bulk clear all environment variables defined in a given environment file.

## Usage

```
xport -e [target environment] -s [shell] -d [environment file directory] -f [environment file]
```

### Command Line Options

* `-e` , `--env`: Specify the target environment. Default: 'dev'
* `-f` , `--file`: Specify the environment file name.
* `-d` , `--dir`: Specify the directory where to search for the environment file.
* `-s` , `--shell`: Specify the target shell. One of: `sh`, `csh`, `fish`, `xonsh`, `powershell`
* `-v` , `--version`: Print version number and exit.
* `-c` , `--clear`: If present `xport` will output syntax to clear the defined environment variables.

### Supported shells

| Shell Type         | Accepted `-s` Option Arguments  |
|:------------------:|:-------------------------------:|
| UNIX/POSIX Shells  | sh, bash, zsh, ksh, ash, dash   |
| PowerShell         | powershell, pwsh, posh          |
| C Shell            | csh, tcsh                       |
| Fish               | fish                            |
| Xonsh              | xonsh                           |

## Command Line Example

The examples below assume that `xport` is in a directory in your `PATH`

### [bash](https://www.gnu.org/software/bash/), [zsh](https://www.zsh.org/), and [ksh](http://www.kornshell.com/)

```bash
source <(xport -e prod -s bash -d ./example/)
```

### [fish](https://fishshell.com/)

```fish
xport -e prod -s fish -d ./example/ | source
```

### [PowerShell](https://microsoft.com/powershell)

```powershell
xport -e qa -s powershell -d ./example/ | Invoke-Expression
```

## Environment File Examples

The program assumes that the environment file is in the format of `KEY=VALUE` and one per line.

See example environment files:

* [`.fake.env`](examples/.fake.env)
* [`.dev.env`](./examples/.dev.env)
* [`.prod.env`](./examples/.prod.env)
* [`.qa.env`](./examples/.qa.env)
* [`.stage.env`](./examples/.stage.env)

## Installation

### Versions and Dependencies

| Software | Version  |
|:--------:|:--------:|
| `dmd`    | v2.101.0 |
| `dub`    | 1.29.0   |

### Build

```
make
```

### Install

```
make install
```

## Licenses 

The primary licensed used in this repository is the [Common Development and Distribution License (CDDL), version 1.0](LICENSES/CDDL-1.0.txt). However there are other licenses used depending on the kind of file.

To avoid ambiguity each file incorporates their applicable license by reference using a comment header that specifies the copyright and license information at the top of the file. The full text of all licenses used in this repository are stored in files under the [`LICENSES`](LICENSES/) directory where the name of the file is the [SPDX License Identifier](https://spdx.org/licenses/) followed by `.txt`, as suggested by the [REUSE Specification](https://reuse.software/spec/). 

If a file cannot include a comment header, the license information should be placed in an adjacent file of the same name with the additional extension `.license`, following the [REUSE Specification](https://reuse.software/spec) (example: `demo.gif.license` if the original file is `demo.gif`).

An example template CDDL comment header for the [D language](https://dlang.org/) is provided below. Use it as a starting point when adding new code to this repository. When adding a comment header to a file, pay attention to the kind of file you are adding it to and use the appropriate comment-style. You _must_ replace the fields enclosed by square brackets `[]` with the relevant information. For fields enclosed by angle brackets `<>`, you _must either_ replace _or_ fully remove the template text. See the [CONTRIBUTING.md](CONTRIBUTING.md) file for more details on how to contribute to this project.

```
/**
*  <one line summary of code or document>
*
*  This file and its contents are distributed under the terms of the
*  Common Development and Distribution License, Version 1.0 (the "License").
*  You may not use this file except in compliance with the License.
*
*  You may obtain a copy of the License in the included file,
*  LICENSES/CDDL-1.0.txt or online at:
*
*     http://www.opensource.org/licenses/cddl1
*
*  See the License for the specific language governing permissions and
*  limitations.
*
*  Copyright: [year] [your name] <your email>
*  License: CDDL-1.0
*  Authors: [your name]
*/
```

The following licenses are used in this repository:

**Code:**
* [Boost Software License](LICENSES/BSL-1.0.txt): Supporting files not tightly coupled to the application code or not highly differentiated, such as simple Makefiles or helper scripts.
* [Common Development and Distribution License](LICENSES/CDDL-1.0.txt): The application code and tightly coupled related files.
* [The Unlicense](LICENSES/Unlicense.txt): Examples.

**Documentation:**
* [Common Documentation License](LICENSES/CDL-1.0.txt): Documentation such as [this](README.md) and other Markdown files.


New contributions to this repository should be licensed under one of the above licenses as well. Integrating code from other sources that use a compatible open-source license is possible but requires prior approval.

## CDDL TL;DR

**Disclaimer**: _This section is a simplified summary of the Common Development and Distribution License 1.0 (CDDL) and is not a replacement for reading the full license. It is not intended to provide legal advice and should not be relied upon as a substitute for legal counsel. For a full understanding of the terms and conditions of the CDDL 1.0, please refer to the original license text available in the [LICENSES/CDDL-1.0.txt](LICENSES/CDDL-1.0.txt) file._
 
 * **Grant of License**: The CDDL grants you a license to use, reproduce, modify, distribute, and sublicense the licensed software, as long as you comply with the conditions outlined in the license.
 * **Conditions**: You must include a copy of the CDDL license with any distributions of the licensed software, and you must make the source code available to anyone who receives a copy of the software. You also cannot use the names of contributors to endorse your own products without their prior written consent.
 * **Termination**: The license may be terminated if you breach any of its conditions.
 * **Disclaimer of Warranty**: The CDDL software is provided "as is," without any warranty, either express or implied.
 * **Limitation of Liability**: The contributors to the CDDL software cannot be held liable for any damages resulting from the use of the software.
 * **Trademarks**: You are not allowed to use the trademarks of contributors to the CDDL software without their prior written consent.
 * **Choice of Law**: The CDDL is governed by the laws of the State of California.
 * **Requirements**: You must comply with any additional requirements specified in the license.
 * **Interpretation**: The terms of the CDDL should be interpreted in a reasonable manner, and any ambiguities should be resolved in favor of licensors and contributors to the software.

**Web Links:**

 * [opensource.org - CDDL 1.0 License Text](https://opensource.org/license/cddl1)
 * [fossa.com - Open Source Licenses 101: The CDDL (Common Development and Distribution License)](https://fossa.com/blog/open-source-licenses-101-cddl-common-development-distribution-license/)
 * [tldrlegal.com - Common Development and Distribution License (CDDL-1.0)](https://www.tldrlegal.com/l/cddl)

---
Copyright © 2023 Drew Adams <<druonysus@opensuse.org>>.

This material has been released under and is subject to the terms of the
Common Documentation License, v.1.0, the terms of which are hereby
incorporated by reference. Please obtain a copy of the License at
https://spdx.org/licenses/CDL-1.0.html and read it before using this
material. Your use of this material signifies your agreement to the terms of
the License.
