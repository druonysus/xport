/**
* Implementes xport's Version class
*   
* This file and its contents are distributed under the terms of the
* Common Development and Distribution License, Version 1.0 (the "License").
* You may not use this file except in compliance with the License.
*
* You can obtain a copy of the License in the included file,
* LICENSES/CDDL-1.0.txt or online at:
*
*     http://www.opensource.org/licenses/cddl1
*
* See the License for the specific language governing permissions and
* limitations.
* 
* Copyright: 2023 Drew Adams <druonysus@opensuse.org>
* License: CDDL-1.0
* Authors: Drew Adams
*/
module xport.ver;
 
class Version
{
    import std.format;

    public int Major;
    public int Minor;
    public int Patch;
    this(int major, int minor, int patch)
    {
        this.Major = major;
        this.Minor = minor;
        this.Patch = patch;
    }
    override
    string toString()
    {
        return format("%d.%d.%d", this.Major, this.Minor, this.Patch);
    }
}
