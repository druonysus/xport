#!/bin/bash
#
# Built xport RPM package
#
#  This file and its contents are distributed under the terms of the
#  Universal Permissive License, Version 1.0 (the "License").
#  You may not use this file except in compliance with the License.
#
#  You may obtain a copy of the License in the included file,
#  LICENSES/UPL-1.0.txt or online at:
#
#     https://opensource.org/license/upl
#
#  See the License for the specific language governing permissions and
#  limitations.
#
#  Copyright: 2023 Drew Adams <druonysus@opensuse.org>
#  License: UPL-1.0
#  Authors: Drew Adams
#
set -x

VERSION=${1:-$(grep "^version " dub.sdl)}
RELEASE=${2:-0}

umask 0022

TEMPDIR=$(mktemp -d)
INSTDIR=$(mktemp -d)

trap "rm -rf $TEMPDIR $INSTDIR" EXIT


mkdir -p $TEMPDIR/{BUILD,BUILDROOT,RPMS,SOURCES,SPECS,SRPMS}
echo "%_topdir $TEMPDIR" > $TEMPDIR/macros

mkdir -p -m 0755 $INSTDIR/usr/bin
mkdir -p -m 0755 $INSTDIR/usr/share/man/man1
mkdir -p -m 0755 $INSTDIR/usr/share/doc/xport

install -m 0755 bin/xport $INSTDIR/usr/bin
install -m 0644 man/xport.1 $INSTDIR/usr/share/man/man1
dos2unix -q -n README.md $INSTDIR/usr/share/doc/xport/README
chmod 0644 $INSTDIR/usr/share/doc/xport/README

(cd $INSTDIR ; tar -czf $TEMPDIR/SOURCES/install-files.tar.gz *)

sed -e "s/@VERSION@/$VERSION/" -e "s/@RELEASE@/$RELEASE/" pkg/rpm/xport.spec.in > $TEMPDIR/SPECS/xport.spec

fakeroot rpmbuild --quiet --define "%_topdir $TEMPDIR" -bb $TEMPDIR/SPECS/xport.spec

cp $TEMPDIR/RPMS/x86_64/*.rpm pkg/rpm
