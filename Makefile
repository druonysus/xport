#
# The main xport Makefile
#
#  This file and its contents are distributed under the terms of the
#  Common Development and Distribution License, Version 1.0 (the "License").
#  You may not use this file except in compliance with the License.
#
#  You may obtain a copy of the License in the included file,
#  LICENSES/CDDL-1.0.txt or online at:
#
#     http://opensource.org/licenses/cddl1
#
#  See the License for the specific language governing permissions and
#  limitations.
#
#  Copyright: 2023 Drew Adams <druonysus@opensuse.org>
#  License: CDDL-1.0
#  Authors: Drew Adams
#
PACKAGE_NAME= $(shell grep -P "^name" dub.sdl | awk -F': ' '{print $$2}')
PACKAGE_VERSION= $(shell grep -P "^version" dub.sdl | awk -F': ' '{print $$2}')
REPO_ROOT= $(shell pwd)
DUB_BUILD_ROOT= $(REPO_ROOT)/bin
RPM_BUILD_ROOT= $(REPO_ROOT)/build/rpm

usage:
	@echo "Usage: make build   : Build xport" >&2
	@echo "       make install : Install the result of 'dub build'" >&2
	@echo "       make clean   : Delete files in build/" >&2
	@false

.PHONY: build
build:
	dub build

rpm:
	cd ./pkg/rpm/ && $(MAKE) $@

deb:
	cd ./pkg/deb/ && $(MAKE) $@

clean:
	rm -rf ./bin/*

man-md:
	test -d $(DUB_BUILD_ROOT)/man1/ || mkdir $(DUB_BUILD_ROOT)man1/
	mandoc -mdoc -T md ./man/xport.1 > ./build/xport.man.md

install: build
	strip $(DUB_BUILD_ROOT)/xport && \
	install -m 700 $(DUB_BUILD_ROOT)/xport ~/.local/bin/
