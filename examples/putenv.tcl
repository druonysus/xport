#!/usr/bin/env -S tclsh
#
# DESCRIPTION
#   A stupidly simple Tcl script to print all environment variables.
#
# GOAL
#   illustrate how we can even use our environment files from
#   `make` to kick off scripts with the right environment variables.
#
# This file and its contents are released into the public domain per
# the terms of The Unlicense (the "License").
#
# You can obtain a copy of the License in the included file,
# LICENSES/Unlicense.txt or online at:
#
#     http://www.opensource.org/licenses/unlicense
#
# See the License for the specific language governing permissions and
# limitations.
#
# Copyright: 2023 Drew Adams <druonysus@opensuse.org>
# License: Unlicense
# Authors: Drew Adams
#
parray env