<!--
    xport's examples README file

  This material has been released under and is subject to the terms of the
  Common Documentation License, Version 1.0 (the "License").
  The terms of which are hereby incorporated by reference.

  Please obtain a copy of the License in the included file,
  LICENSES/CDL-1.0.txt or online at:

      https://spdx.org/licenses/CDL-1.0.html

  Read the full License text before using this material. Your use of this
  material signifies your agreement to the terms of the License.

  Copyright: 2023 Drew Adams <druonysus@opensuse.org>
  License  : CDL-1.0
  Authors  : Drew Adams
-->
# `xport` Examples

---
Copyright © 2023 Drew Adams <<druonysus@opensuse.org>>.

This material has been released under and is subject to the terms of the
Common Documentation License, v.1.0, the terms of which are hereby
incorporated by reference. Please obtain a copy of the License at
https://spdx.org/licenses/CDL-1.0.html and read it before using this
material. Your use of this material signifies your agreement to the terms of
the License.